<?php

use Nette\Database\Connection;

class HomepagePresenter extends BasePresenter
{
private $database;

	public function __construct(Nette\Database\Connection $database)
	{
		$this->database = $database;
	}
        
        public function beforeRender() {
                $this->template->product = $this->database->table('product')->order('id DESC');
                $this->template->social = $this->database->table('social');
        }
        
        public function actionDelete($id)
        {
            $this->database->table('product')->where('id = ?', $id)->limit(1)->delete();
            $this->redirect('Homepage:');
        }
        
        public function renderCategory($id = 0)
	{
    	$this->template->category = $this->database->table('product')->where('category_id = ?', $id)->order('id DESC');
	}
        
        public function renderProduct($id = 0)
	{
    	$this->template->product = $this->database->table('product')->where('id = ?', $id)->order('id DESC');
	}

}

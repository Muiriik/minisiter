<?php

use Nette\Application\UI\Form, Nette\Database,
	Nette\Security as NS;

class AdminPresenter extends BasePresenter
{
private $database;

	public function __construct(Nette\Database\Connection $database)
	{
		$this->database = $database;
	}
        
        public function beforeRender() 
        {
                $this->template->product = $this->database->table('product');
                $this->template->social = $this->database->table('social');
                if (!$this->getUser()->isLoggedIn()) {
                    $this->redirect('Sign:in');
                }
        }
        
        public function renderProduct($id = 0)
	{
    	$this->template->product = $this->database->table('product')->where('id = ?', $id);
	}
        
        protected function createComponentAddProduct()
        {
            $form = new Form;
            
            $form->addText('product_name', 'Název produktu')
                    ->setAttribute('placeholder', 'Nazev produktu')
                    ->setAttribute('class', 'form-control')
                    ->addRule(Form::FILLED, 'Pole <strong>"název produktu"</strong> musí být vyplněné! Produkt se musí nějak jmenovat, ne?');
                       
            $form->addRadioList('category_id', 'Kategorie', array(
                '1' => 'Pánské',
                '2' => 'Dámské',
                '3' => 'Unisex'
            ))
                    ->addRule(Form::FILLED, 'Musíte vybrat <strong>kategorii</strong>, do které se produkt zařadí.');
            
            $form->addText('price', 'Cena')      
                    ->setOption('description', 'Toto číslo zůstane skryté')
                    ->setAttribute('placeholder', 'Cena produktu v Kc')
                    ->setAttribute('class', 'form-control')
                    ->addRule(Form::FILLED, 'Pole <strong>cena</strong> musí být vyplněné!')
                        ->addRule(Form::INTEGER, '<strong>Cena</strong> musí být číselná!');
            
            $form->addTextArea('description', 'Popis produktu')
                    ->setAttribute('placeholder', 'Popis produktu')
                    ->addRule(Form::FILLED, 'Bylo by dobré, kdyby ste doplnil alespoň nějaké základní informace o produktu v poli <strong>"popis produktu"</strong>.');
            
            $form->addText('buy_product_url', 'Odkaz')
                    ->setAttribute('placeholder', 'Odkaz ke koupi')
                    ->setAttribute('class', 'form-control');
            
            $form->addText('preview_img_big', 'Adresa, která se zobrazí')
                    ->setAttribute('placeholder', 'Adresa, ktera se zobrazi')
                    ->setAttribute('class', 'form-control');
            
            $form->addText('preview_img_small', 'Adresa, která se zobrazí')
                    ->setAttribute('placeholder', 'Adresa, ktera se zobrazi')
                    ->setAttribute('class', 'form-control');
            
            $form->addText('other_img_big', 'Adresa, která se zobrazí')
                    ->setAttribute('placeholder', 'Adresa, ktera se zobrazi')
                    ->setAttribute('class', 'form-control');
            
            $form->addText('other_img_small', 'Adresa, která se zobrazí')
                    ->setAttribute('placeholder', 'Adresa, ktera se zobrazi')
                    ->setAttribute('class', 'form-control');
            
            $form->addSubmit('send', 'Přidat')
                    ->setAttribute('class', 'btn btn-primary');
            
            $form->onSuccess[] = $this->addProductSubmitted;
            
            
            return $form;                      
        }
        
        public function addProductSubmitted(Form $form)
        {
            $product = $form->getvalues(TRUE);
            $arr = array(
                'product_name' => $product['product_name'],
                'category_id' => $product['category_id'],
                'price' => $product['price'],
                'description' => $product['description'],
                'buy_product_url' => $product['buy_product_url'],
                'preview_img_big' => $product['preview_img_big'],
                'preview_img_small' => $product['preview_img_small'],
                'other_img_big' => $product['other_img_big'],
                'other_img_small' => $product['other_img_small'],          
                'date' => new DateTime()
            );

            $this->database->table('product')->insert($arr);
            $this->flashMessage('Přidáno', 'success');
            $this->redirect('Admin:');  
        }
        
        protected function createComponentSocial()
        {
            $form = new Form;
            $form->addText('facebook')
                    ->setAttribute('class', 'form-control');
            
            $form->addText('twitter')
                    ->setAttribute('class', 'form-control');
            
            $form->addText('twitter_id')
                    ->setAttribute('class', 'form-control');
            
            $form->addText('gplus')
                    ->setAttribute('class', 'form-control');
            
            $form->addSubmit('save')
                    ->setAttribute('class', 'btn btn-primary');
            
            $form->onSuccess[] = $this->socialUpdate;
            
            return $form;
        }
        
        
        public function socialUpdate(Form $form)
        {
            $social = $form->getvalues(TRUE);
            $arr = array(
                'facebook' => $social['facebook'],
                'twitter' => $social['twitter'],
                'twitter_id' => $social['twitter_id'],
                'twitter_active' => $social['twitter_active'],
                'gplus' => $social['gplus'],
            );
            $this->database->table('social')->update($arr);
            $this->flashMessage('Aktualizováno', 'success');
            $this->redirect('Admin:'); 
        }
}



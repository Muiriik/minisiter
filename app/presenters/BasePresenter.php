<?php

/**
 * Base presenter for all application presenters.
 */
use Nette\Database\Connection;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{
        private $database;

	public function __construct(Nette\Database\Connection $database)
	{
		$this->database = $database;
	}
        
        public function beforeRender() {
                $this->template->social = $this->database->table('social');
        }
        
        public function handleSignOut()
        {
            $this->getUser()->logout();
            $this->redirect('Sign:in');
        }
}
